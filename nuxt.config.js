import axios from 'axios';

const dynamicRoutes = () => {
  const routes = axios
    .get('https://api.docteur-frey-esthetique.fr/wp-json/wp/v2/pages')
    .then(res => {
      return res.data.map(page => `/${page.slug}`);
    });

  console.log(routes);

  return routes;
};

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Philippe Frey',

    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Medecine esthétique à Montauban'
      },
      {
        hid: 'og:type',
        name: 'og:type',
        content: 'website'
      },
      {
        hid: 'og:url',
        name: 'og:url',
        content: process.env.WEBSITE_URL
      },
      {
        hid: 'og:title',
        name: 'og:title',
        content: 'Médiévistes'
      },
      {
        hid: 'og:site_name',
        name: 'og:site_name',
        content: 'Médiévistes'
      },
      {
        hid: 'og:locale',
        name: 'og:locale',
        content: 'fr'
      },
      {
        hid: 'og:image',
        name: 'og:image',
        content: `${process.env.WEBSITE_URL}img-accueil/og.jpg`
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=DM+Sans&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Nanum+Myeongjo&display=swap'
      }
    ],
    script: [
      {
        src:
          'https://cdn.polyfill.io/v2/polyfill.min.js?features=IntersectionObserver,IntersectionObserverEntry',
        charset: 'utf-8'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/vue-observe-visibility.client.js'],
  /*
   ** Nuxt.js dev-modules
   */

  buildModules: [
    [
      '@nuxtjs/dotenv',
      {
        /* module options */
      }
    ],
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-157707346-4'
      }
    ]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  generate: {
    routes: dynamicRoutes
  }
};

module.exports = {
  env: {
    API_DF: process.env.API_DF
  }
};
