export const state = () => ({
  pages: [],
  menu: [],
  accueil: [],
  traitements: [],
  footer: [],
  isLoading: true
});

export const mutations = {
  updatePages: (state, pages) => {
    state.pages = pages;
  },

  updateMenu: (state, menu) => {
    state.menu = menu;
  },

  updateAccueil: (state, accueil) => {
    state.accueil = accueil;
  },

  updateTraitements: (state, traitements) => {
    state.traitements = traitements;
  },

  updateFooter: (state, footer) => {
    state.footer = footer;
  },

  updatePreloader: (state, isLoading) => {
    state.isLoading = isLoading;
  }
};

export const actions = {
  async getPages({ state, commit }) {
    if (state.pages.length) return;
    try {
      let pages = await fetch(
        `https://api.docteur-frey-esthetique.fr/wp-json/wp/v2/pages`
      ).then(res => res.json());
      pages = pages
        .filter(el => el.status === 'publish')
        .map(({ id, slug, title, excerpt, date, tags, content, acf }) => ({
          id,
          slug,
          title,
          excerpt,
          date,
          tags,
          content,
          acf
        }));
      // where are you? data?
      // console.log(pages, "pages");
      commit('updatePages', pages);
    } catch (err) {
      console.log(err);
    }
  },

  async getMenu({ state, commit }) {
    if (state.menu.length) return;
    try {
      let menu = await fetch(
        `${process.env.API_DF}wp-json/menus/v1/menus/appnav`
      ).then(res => res.json());
      menu = menu.items;
      // where are you? data?
      // console.log(menu, "menu");
      commit('updateMenu', menu);
    } catch (err) {
      console.log(err);
    }
  },

  async getAccueil({ state, commit }) {
    if (state.accueil.length) return;
    try {
      let accueil = await fetch(
        `${process.env.API_DF}wp-json/acf/v3/pages`
      ).then(res => res.json());
      accueil = accueil
        .filter(el => el.id === 9)
        .map(({ id, title, acf }) => ({
          id,
          title,
          acf
        }));
      // where are you? data?
      // console.log(accueil, "accueil");
      commit('updateAccueil', accueil);
    } catch (err) {
      console.log(err);
    }
  },

  async getTraitements({ state, commit }) {
    if (state.traitements.length) return;
    try {
      let traitements = await fetch(
        `${process.env.API_DF}wp-json/wp/v2/traitements`
      ).then(res => res.json());
      traitements = Object.values(traitements).map(
        ({ id, title, slug, excerpt, date, tags, acf }) => ({
          id,
          title,
          slug,
          excerpt,
          date,
          tags,
          acf
        })
      );
      // where are you? data?
      // console.log(traitements, "trait");
      commit('updateTraitements', traitements);
    } catch (err) {
      console.log(err);
    }
  },

  async getFooter({ state, commit }) {
    if (state.footer.length) return;
    try {
      let footer = await fetch(
        `${process.env.API_DF}wp-json/acf/v3/pages`
      ).then(res => res.json());
      footer = footer
        .filter(el => el.id === 126)
        .map(({ id, acf }) => ({
          id,
          acf
        }));
      // where are you? data?
      // console.log(footer, "footer");
      commit('updateFooter', footer);
    } catch (err) {
      console.log(err);
    }
  }
};
